//? name=Save REd Java Sources, shortcut=Ctrl+Shift+S, help=Salva i sorgenti dei file in una cartella

import jeb.api.IScript;
import jeb.api.JebInstance;
import jeb.api.dex.Dex;

import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class JEBSourceSaver implements IScript {

    private final String packageNameToSave = "Lcom/er/mo/apps/mypasswords";
    private String savePath = "\\Reversed Sources\\";

    public void run(JebInstance jeb) {
        String inputPath = jeb.getInputPath();
        savePath = inputPath.substring(0, inputPath.lastIndexOf("\\")) + savePath;

        jeb.print("\nSalvataggio dei file .java (reverse engineered).");
        List<String> classesNames = getAllClasses(jeb);
        List<String> folders = getFolderTree(classesNames);

        if(!classesNames.isEmpty() && !folders.isEmpty()) {
            try {
                for (Iterator<String> iter = folders.listIterator(); iter.hasNext(); ) {
                    String folder = savePath + iter.next();
                    File f = new File(folder);
                    f.mkdirs();
                }
                jeb.print("Inizio salvataggio file...");
                jeb.print("Path: " + savePath);
                for (Iterator<String> iter = classesNames.listIterator(); iter.hasNext(); ) {
                    String currentFile = iter.next();
                    File file = new File(savePath+currentFile+".java");
                    if(!file.exists()) {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file.getAbsolutePath(), false);
                    String decompiledClass = jeb.decompileClass(currentFile);
                    if(decompiledClass != null) {
                        fw.write(decompiledClass);
                    }
                    fw.flush();
                    fw.close();
                }
                jeb.print("Salvataggio avvenuto con successo.");
            } catch (IOException e) {
                jeb.print(e.getMessage());
            }
        } else {
            jeb.print("ERRORE: Package non trovato.");
        }
    }

    public List<String> getAllClasses(JebInstance jeb){
        Dex dexFile = jeb.getDex();

        /* True è usato in quanto permette di reperire i dati modificati
         * e non quelli originali dell'apk */
        List<String> names = dexFile.getClassSignatures(true);
        List<String> result = new ArrayList<String>();

        // Rimuovi le classi di Android.
        for (Iterator<String> iter = names.listIterator(); iter.hasNext(); ) {
            String a = iter.next();
            if (a.startsWith(packageNameToSave)) {
                result.add(a.substring(0, a.lastIndexOf(';')));
            }
            iter.remove();
        }
        return  result;
    }

    public List<String> getFolderTree(List<String> classList){
        List<String> folders = new ArrayList<String>();
        for (int i = 0; i < classList.size(); i++){
            String a = classList.get(i).substring(0, classList.get(i).lastIndexOf('/'));
            folders.add(a);
        }
        return folders;
    }
}